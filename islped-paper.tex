% THIS IS SIGPROC-SP.TEX - VERSION 3.1
% WORKS WITH V3.2SP OF ACM_PROC_ARTICLE-SP.CLS
% APRIL 2009
%
% It is an example file showing how to use the 'acm_proc_article-sp.cls' V3.2SP
% LaTeX2e document class file for Conference Proceedings submissions.
% ----------------------------------------------------------------------------------------------------------------
% This .tex file (and associated .cls V3.2SP) *DOES NOT* produce:
%       1) The Permission Statement
%       2) The Conference (location) Info information
%       3) The Copyright Line with ACM data
%       4) Page numbering
% ---------------------------------------------------------------------------------------------------------------
% It is an example which *does* use the .bib file (from which the .bbl file
% is produced).
% REMEMBER HOWEVER: After having produced the .bbl file,
% and prior to final submission,
% you need to 'insert'  your .bbl file into your source .tex file so as to provide
% ONE 'self-contained' source file.
%
% Questions regarding SIGS should be sent to
% Adrienne Griscti ---> griscti@acm.org
%
% Questions/suggestions regarding the guidelines, .tex and .cls files, etc. to
% Gerald Murray ---> murray@hq.acm.org
%
% For tracking purposes - this is V3.1SP - APRIL 2009

\documentclass{sig_alternate}

\usepackage{framed}
\usepackage{amsmath}
\usepackage{subfig}
\usepackage{enumerate}
%\usepackage[shortlabels]{enumitem}
\usepackage{array}
\usepackage{mathbbol}
\usepackage{comment} 
\makeatletter  
 \let\@copyrightspace\relax  
\makeatother  

\begin{document}
\conferenceinfo{ISLPED}{'14 La Jolla, CA, USA}
\graphicspath{{isca-fina-data/}}
%\renewcommand{\bibfont}{\normalfont\small}
\newcounter{copyrightbox}
\title{Tag Check Elision}



\maketitle
\begin{abstract}
For set-associative caches, accessing cache ways in parallel results in significant energy waste, as only one way contains the desired data. In this paper, we propose Tag Check Elision (TCE): a non-speculative approach for accessing set-associative caches without a tag check to save energy. 


TCE can eliminate up to 86\% of the tag checks (67\% on average), without sacrificing any performance. These direct accesses to a 4-way set-associative data cache under TCE result in up to 56\% and 85\% data cache and Data Translation Look-aside Buffer (DTLB) dynamic energy saving, respectively. 
\end{abstract}


% A category with the (minimum) three required fields
\category{C.1.0}{Processor Architecture}{General}
%A category including the fourth, optional field follows...
\category{B.3.2}{Memory structure}{Design Styles}[Cache memories]
\terms{Design, Experimentation}

%\keywords{ACM proceedings, \LaTeX, text tagging}

\section{Introduction}
 
Caches have been playing an important role in efficiently bridging the speed gap between memory and CPU for decades. However, they consume a large fraction of the on-die area along with up to 45\% of core power \cite{micro_talk_power}. In addition, industrial sources and early research report that 3-17\% of core power can be consumed by Translation Look-aside Buffer (TLB) \cite{micro_talk_power, reduce_tlb, gen_phy_addr}. In the many-core era, the power quota for each core is very limited, which requires simple core logic design along with more power efficient cache and TLB implementation without hurting performance.
 
Set-associative caches dominate the cache design in current commercial processors, as they provide higher hit rates, resulting in better performance. However, they require parallel way read and energy-hungry tag comparison. As a result, much of the energy is wasted on accessing data that is discarded after tag check.
 

In this paper, we propose Tag Check Elision (TCE), a hardware approach to access set-associative caches without tag checks. TCE determines the correct cache way early in the pipeline by doing a simple bound check that relies on the base register and offset associated with the memory instruction. In the same vein, the TLB access can also be eliminated in a physically-tagged cache. TCE memoizes the accessed cache ways and relies on a bounds check to decide if the later access is to the same line as an earlier access. The bounds check occurs as soon as the virtual address is available, and incurs no additional pipeline delay or performance degradation. 


 The results show that 35\% to 86\% (67\% on average) of memory access, in SPEC CPU2006 benchmarks \cite{spec2006}, can perform direct access to the cache. The cache dynamic energy saving is 15\% to 56\%,  with 33\% on average. The DTLB dynamic energy saving is 34\% to 85\% (66\% on average). TCE outperforms two types of way prediction, including MRU and perfect prediction, in terms of Energy Delay Product (EDP). 
 


The rest of the paper is organized as follows: The background and insight that motivates our work is presented in Section \ref {motivation}. The design of the TCE approach is described in Section \ref{tce-design}. 
Experimental results are presented in Section \ref{evaluation}.  Related work is discussed in Section \ref{related_work} and Section \ref{conclusion} concludes the paper.

\section{Motivation} \label{motivation}

For set-associative caches, all tags in a set must be checked to decide which way contains the requested data. To provide fast access in level one cache, all tags and data ways in a set are accessed in parallel, while only one way has the requested data. This redundant tag check and data access results in much energy waste. For example, in a 4-way set-associative cache, 4 tag checks and 4 data block reads are performed. In contrast, TCE reduces the cache access to just one data block read, eliding all tag checks and 3 other data block reads.

TCE is inspired by how memory addresses are generated. The example code in Figure \ref{fig:code}(a) adds arrays $b$ and $c$ to form a new array $a$. The addresses to the array elements are computed by adding offsets to base registers, as shown in Figure \ref{fig:code}(b)), where the base registers are $rbp$, $r13$ and $r12$, respectively. The index register $rsi$ is incremented by 4 in each loop iteration, providing the offset for the array addresses.

The addresses for these arrays comprise static base registers and an increasing index register. As the cache line size (typically 64B) is much smaller than the physical page size (typically 4KB), the same cache line access can be determined just by comparing the virtual addresses. However, this virtual address comparison cannot be performed until address generation. Fortunately, if the base address register is unchanged (e.g. base registers shown in Figure \ref{fig:code}(b)), the same cache line determination could be simplified to just compare the offsets (e.g. register $rsi$ ) with a bound check. Based on this observation, we design a mechanism to determine same cache line accesses by keeping a cache line record for the base register and comparing the offset value. Once the same cache line access is detected, the access to the cache can be performed directly without any tag check or TLB access. This type of access elides tag checks, and thus we name this approach as Tag Check Elision. The sufficient condition for the same cache line determination is:
\begin{itemize}
\itemsep0em
\item The same base register value,
\item And the offset is within the cache line bounds.
\end{itemize}



Overall, the advantages of our TCE approach are: 

\begin{enumerate}[(a)]
\itemsep0em
\item The stored cache line information and the offset bounds are read the same time as performing register value read, as they are indexed by base register id instead of register value or address; 
\item The offset comparison is performed in parallel with the address generation, which dose not add any delays to the critical data path; 
\item TCE completely elides the tag check for the determined same cache line access; 
\item On the direct access determined by TCE, the access to the TLB is bypassed to save more energy. 
\end{enumerate}

\begin{figure}[t]
\begin{center}
\fbox{\includegraphics[width=7cm]{fig-code.pdf}}
\end{center}
\caption{{\label{fig:code}}Loop to add two arrays and its corresponding instructions in X86 ISA.}
\end{figure}

\section{TCE design} \label{tce-design}
\subsection{Cache way memoization }

To memoize the accessed cache lines, cache way records (CWRs) are added to the processor, as shown in Figure \ref{fig:cache_way}. The number of CWRs matches the number of architected fixed-point registers in the ISA, as one record is kept for one register that will be used as base address register. This CWR design allows it to be indexed by the register id and be accessed early, the same time as the register read in the pipeline. 

Each CWR includes three fields, namely \textit{valid}, \textit{bound}, and \textit{cache pointer}. The valid field indicates if the corresponding cache block information is valid or not. The bound field gives the offset range that is located within the current cache line with the same base register value.  As the range length is exactly the cache line size, we can just keep the lower bound in the record to save space. The cache pointer records the last cache line accessed based on the current base register. 





\begin{figure}[t]
\begin{center}

\includegraphics[width=6.5cm]{fig-cwr-hpca.pdf}
\end{center}
\caption{{\label{fig:cache_way}}Example of cache way records organization.}
\end{figure}


\subsection{Walkthrough example}

\begin{table*}
\centering
\begin{tabular}{|m{0.3cm}|m{3cm}|m{10cm}|m{2.5cm}|}
\hline
\centering & \textbf{Instruction} & \textbf{Action} & \textbf{Record state}\\
\hline
1 & Mov 4(\%rdx), \%rcx & {\bf [Miss]:} Access the cache in normal mode, get the way pointer.
& \fbox{1}\fbox{ -8 }\fbox{ 3 }\\
\hline
2 &Add 8(\%rdx), \%rcx & {\bf[Hit]:} The offset 8 is within the bounds between -8 and 56 (-8+64). Then access the cache block through direct access. & \fbox{1}\fbox{ -8 }\fbox{ 3 }\\
\hline
3 & Mov \%rcx, 64(\%rdx)& {\bf[Miss]:} The offset 64 is out of bounds between -8 and 56. Access the cache in normal mode and rebuild the record. & \fbox{1}\fbox{ 56 }\fbox{ 2 }\\
\hline
4 & Mov \%rax, \%rdx & {\bf[Invalidation]:} The value of the base register \textit{rdx} has changed. The corresponding cache way record for register \textit{rdx} must be invalidated. & \fbox{0}\fbox{ X }\fbox{ X }\\
\hline
\end{tabular}
\caption{ An example to show how the cache way record works, using X86 ISA. Initially, register \textit{rdx}'s value is \textit{0x40043e8}, the corresponding CWR for register \textit{rdx} is not valid, \fbox{0}\fbox{ X }\fbox{ X }. X indicates that the value is meaningless as the valid field is 0. The cache line size in the example is 64 bytes.}
\label{table:work-example}
\end{table*}

An example of how the TCE mechanism works is shown in Table \ref{table:work-example}. This example shows how the register \textit{rdx} and its corresponding CWR changes.

At the beginning, we assume that the value of the register \textit{rdx} is \textit{0x40043e8} and the CWR for register \textit{rdx} is invalid. The instruction in the first row accesses the memory whose address comprises the value in register \textit{rdx} and an immediate number \textit{4}. As the valid bit in the CWR is 0, the cache access is performed in the normal mode (probing all ways in parallel) and the cache way number  (e.g. 3) of current access is obtained to build the CWR. According to register \textit{rdx}'s value and the current offset value, the bounds for the current accessed cache line are calculated, which are -8 and 56 (-8 + cache line size, 64). Thus, the CWR state for \textit{rdx} becomes \fbox{1}\fbox{ -8 }\fbox{ 3 } after first instruction.



When executing the second instruction, the CWR is valid and the offset 8 is within the bounds between -8 and 56. Direct access to the desired cache way is performed to fetch data. This case is what we expect, accessing cache directly and bypassing DTLB access to save dynamic energy.

On the third instruction, the offset 64 is out of bounds of the current CWR. In this situation, the cache access must be performed in normal mode, and the CWR is rebuilt after finishing cache access (assuming the accessed cache way number is 2). The CWR is invalidated after the last instruction as the value of register \textit{rdx} is modified.

For simplicity of illustration, this example uses immediate values as offsets to describe our proposed approach. However, this approach works the same for the offsets that are stored in the register file, as shown in Figure \ref{fig:code}.

\subsection{TCE enhancements}

\subsubsection{Multiple records.}
Keeping one record for each register will possibly suffer from capacity miss. With multiple records, more cache line information can be kept. If we keep $N$ records for one register, then we can track the last $N$ cache line that have been accessed based on the same register value. However, keeping too many records will complicate the management of the CWRs. 

In our design, initial experiments showed that provisioning \textit{two} CWRs (Double Record, DR) for each register forms a reasonable compromise between complexity and performance.

\subsubsection{Register value tracking.}

\begin{figure}[t]
\begin{center}
\fbox{\includegraphics[width=7cm]{fig-code2-line.pdf}}
\end{center}
\caption{{\label{fig:code2}}Loop to sum the array \textit{a} and its corresponding instructions in X86 ISA.}
\end{figure}

As shown in Figure \ref{fig:code2}, the access to the array only uses base register $rax$. The base register is incremented by $4$ in each iteration, causing CWR rebuilding. To deal with the small changes to the register,  another optimization is added to keep track of the register value to avoid unnecessary invalidation; this applies whenever the accesses are still located in the same cache line.

This optimization is called Register Value Tracking (RVT), which keeps the CWR valid by tracking the register value and adjusting the corresponding bound field. On the instruction \textit{add \$0x4, \%rax }, we update the CWR by subtracting the bound with the same immediate $0x4$, instead of invalidating. Thus, the CWR still points to the same cache line and the next access has chance to hit.

\subsubsection{Backup buffer.}
\begin{figure}[t]
\begin{center}
\includegraphics[width=8cm]{fig-buffer-hpca.pdf}
\end{center}
\caption{{\label{fig:buffer}}Example of backup buffer design for the CWRs.}
\end{figure}

When the register value changes, the corresponding CWR entry must be invalidated. However, it is possible that this discarded CWR would be useful later. To prevent unnecessary CWR rebuilding, a backup buffer can be added to buffer the CWRs that are discarded because of the register value change. 


As the CWR is strongly coupled with the register value and the offset, each item in the backup must contain the register value, as shown in Figure \ref{fig:buffer}. The backup can be organized as an inexpensive direct-mapped cache. The tag is the register value, and the data is the corresponding CWR item. 


\subsubsection{Energy modeling.} The energy consumption of CWRs check, register value tracking, and backup buffer are carefully modeled and integrated into McPAT \cite{Mcpat:micro}.


\subsection{Coherence and correctness}


To guarantee correct access to the cache without tag check, the CWRs must be kept coherent with the cache lines. Once a cache line is evicted or invalidated, the CWRs entries that point to this cache line must be invalidated.
Instead of using energy-intensive associative lookups as \cite{witchel2001direct}, we keep backward pointers in each cache line.



For each cache line, we add a vector to indicate which register's CWRs have pointers pointing to this cache line, one bit for each register. When the cache line is accessed in normal mode to build a CWR, the vector bit for the CWR will be set. As the number of fixed-point registers is relatively small, the cost of the vector bits will be fairly low.

For small number of backup buffer entries, the same vector bits mechanism between cache line and the CWR could be applied to the CWR and the backup buffer. It is shown, in the next section, that 16-entry backup buffer is a desire option, considering the overhead and coherence maintaining complexity.

For other events that will possibly make CWR entries go stale, for example, memory map change, page access permission change, and thread context switch, TCE invalidates all of the CWRs. TCE also bypasses DTLB lookups; hence, any DTLB replacement will invalidate all CWRs to guarantee correctness.



\subsection{TCE design in the pipeline}

\begin{figure}[t]
\begin{center}

%\includegraphics[width=8cm]{fig-pipe-new-islped.pdf}
\includegraphics[width=8cm]{fig-pipeline-islped.pdf}
\caption{{\label{fig:pipeline}}Pipeline integrated with TCE.}
\end{center}
\end{figure}

As illustrated in Figure \ref{fig:pipeline}, the structures added to implement the TCE are shaded in the pipeline. For simplicity, the pipeline components that are not related to the TCE approaches are not shown in the figure. The register-id-indexed CWR is accessed the same time as the register file read, and the bounds check for the offset is finished in parallel with address generation, by dedicated comparator \textit{CMP}. The bounds check results and the cache way information are sent to the cache to decide which mode should be adopted to finish the data access. The iALU component is dedicated for RVT, and the backup buffer is accessed in WB stage. 

The direct access information is stored in EXE/MEM latch before reaching MEM stage.  The cache way information can be bypassed to EXE/MEM latch to satisfy back-to-back access, like the first and second instruction in Table \ref{table:work-example}. When cache lines get evicted, it also check the EXE/MEM latch to invalidate the corresponding TCE cache access.

%Besides the added structure, minor modifications are needed to the cache control inputs to enable a direct access to the cache way specified by the CWR entry.  Beyond these changes, the simple TCE design does not change any function of the other pipeline components, and the energy costs to access the small CWR and perform the bounds check are negligible compared with the data cache access.




\section{Evaluation} \label{evaluation}

\subsection{System configuration}

\begin{table}[t]
\centering
\begin{tabular}{|m{3.3cm}|m{4.5cm}|}
\hline
\textbf{Parameters} & \textbf{Value}\\
\hline
\texttt{Processor} & One core, X86 ISA, In-order \\
%\hline
%Instruction issue & 2 issues per cycle \\
%\hline
%L1 ICache & 32K, 4-way, 2 cycles, 64 bytes\\
\hline
\texttt{Base L1 DCache} &32K, 4-way, 3 cycles, 64 B\\
\hline
\texttt{L2 Cache} & 2MB, 16-way, 12 cycles, 64 B\\
%\hline
%Memory access latency & 80 cycles + 4 cycles per 8 bytes \\

\hline
\texttt{DTLB} & 64 entries, fully-associative\\
\hline
\texttt{Basic CWR} & 32 bits, 16 entries\\

\hline
\texttt{DCache back pointer}& 16 bits per cache line, 1KB in total \\
\hline
\end{tabular}
\caption{System configuration parameters.}
\label{table:configuration}
\end{table}

The system configuration in our evaluation is listed in Table \ref{table:configuration}. Our base architecture is a single core X86 in-order processor. 
As tens to hundreds of cores are integrated into one chip, simple cores are appealing due to their area and power efficiency (e.g. Intel{\textregistered} Xeon Phi co-processor \cite{intel_phi}). As TCE deals with L1 caches, it is scalable to multi-cores. TCE can also be adapted to out-of-order cores, but that is beyond the scope of this paper.


We use Gem5 \cite{Binkert:gem5} in Syscall Emulation mode (SE mode) for detailed architecture simulation, with Simpoints \cite{sherwood2002simpoints} for fast simulation. TCE mechanism and a simple DTLB simulation component are added to Gem5.

The test programs come from SPEC CPU2006 \cite{spec2006}, with input data size \textit{train}. The benchmarks are compiled and statically linked with GCC-4.7.2, G++-4.7.2 and gfortran-4.7.2. We integrated TCE component into McPAT \cite{Mcpat:micro} to estimate the whole processor energy dissipation.

The baseline systems that we compared with are two way prediction schemes, MRU and perfect prediction, where perfect prediction is not realistic and just for reference. As way prediction incurs performance degradation, we adopt Energy Delay Product (EDP) as metric for comparison.



\subsection{CWR hit rate}
%\subsubsection{CWR hit rate.}
\begin{figure*}[!ht]
\begin{center}
\includegraphics[width=16cm]{fig-hit-rate-2.pdf}
\end{center}
\caption{{\label{fig:different-opts}}CWR hit rate for data cache access under different optimizations. }
\end{figure*}


The total CWR hit rates for SPEC benchmarks under different optimization is shown in Figure \ref{fig:different-opts}. We evaluate the effectiveness of the base design and optimizations by cumulatively adding optimizations and increasing the backup buffer size. 

Generally, the hit rate increases as more optimizations are applied over the base design, and with more backup buffer entries. On average, the base design can capture 40.95\% of the memory access, 36.96\% and 43.77\% for SPECint and SPECfp, respectively. 
The effectiveness of the basic design and optimizations vary widely among different benchmarks. The reason for this difference is two-fold: the data access pattern in the program and the binary code compiled and optimized by compilers. The data access pattern depends on the problem that the program is solving and how the program is written. 





The most significant difference among the benchmarks comes with the RVT optimization. \textit{Libquantum} has more than 50\% increase, and another five benchmarks from SPECint and three from SPECfp have more than 20\% increase. In contrast, \textit{mcf}, \textit{cactusADMx} have just around 1\% increase.


With DR, RVT and a 16-entry backup buffer, up to 86\% of accesses (\textit{hmmer}) can perform direct access, with 67\% on average. When increasing the backup buffer entry number to 1024, the benefit over 16-entry one is just 4.7\% hit rate increase on average, while this huge backup buffer will cause significant area and energy overhead.






\subsection{Energy}
\subsubsection{Data cache and DTLB dynamic energy saving.}
\begin{figure*}[!ht]

\begin{center}
\subfloat{\includegraphics[width=8cm]{fig-tce-energy-saving.pdf}}
\
\
\
\subfloat{\includegraphics[width=7.6cm]{fig-breakdown-isca.pdf}}
\end{center}
\caption{{\label{fig:energy-saving}} Data cache and DTLB dynamic energy saving (left), and TCE access breakdown (right). }
\end{figure*}


As shown in the last section, a large number of backup buffer entries offer marginal benefit while increasing complexity energy consumption. The rest results are be based on {\bf{Base + DR + RVT + 16 BBs}}.

The cache and DTLB dynamic energy saving for SPEC CPU2006 benchmarks are shown in Figure \ref{fig:energy-saving} (left). The energy savings for cache dynamic energy vary widely, with around 55.69\% for \textit{sphinx3} to just 15.01\% for \textit{gamess}. Generally, the proposed TCE mechanism reduces the cache and DTLB dynamic energy by 32.72\% and 66.38\% on average, respectively.

Higher hit rate in the CWR does not necessarily result in higher cache dynamic energy saving. Because write to the data cache will first check tag then write to the data array. TCE writes can only save the tag check energy, whereas TCE read can save energy on both tag check and data array read. For example, \textit{milc} enjoys higher total hit than \textit{h264ref}, 84.09\% and 79.44 \%, respectively. However, the cache dynamic energy saving for these two benchmarks are 43.60\% and 49.67\%. The reason is that TCE read in \textit{h264ref} accounts for about 68\% of the total memory access while that in \textit{milc} is just around 60\%, shown in Figure \ref{fig:energy-saving} (right).



As the DTLB miss rate is pretty low and the energy consumption for TLB lookup is much higher than the TLB replacement, the DTLB access energy accounts for around 99\% percent of the DTLB energy. Thus, the percentage of dynamic DTLB energy saving is almost the same as the hit rate in the CWR.


\subsubsection{ Area and energy overhead.} The biggest area overhead comes from the back pointer vector, with 1KB capacity in total. However, the total area overhead is less than 0.1\% of the total chip area. In addition, TCE design adopts small structures for CWR and backup buffer, compared with data cache. The energy cost to access these structures and doing optimization is very low, less than 0.5\% of the data cache energy. 

\subsection{Comparison with way prediction}

In this section, the TCE approach is compared with the most closely-related prior work, way prediction, which similarly does not rely on the compiler or ISA modification. 


Two way prediction schemes are included to give a better comparison:
(a) Ideal Most Recent Used (iMRU): MRU technique predicts the last accessed way in a set to be the next. In this evaluation, we assume an ideal MRU, where no delays will be added to the critical path and no energy will be consumed to access the predict bit before cache access. (b)Perfect Prediction (PP): The PP scheme can correctly predict every access without any cost (area or energy cost), which only miss on data cache miss. This kind of prediction cannot be achieved by any prediction scheme. This is included \textit{ONLY} as a reference to the best-case potential of all previously-proposed way prediction approaches.




Energy delay product (EDP) is reported to compare way prediction with the TCE approach.


\subsubsection{Performance.}



\begin{figure}[t]
\begin{center}

\includegraphics[width=8.3cm]{fig-predict-accuracy-big.pdf}

\includegraphics[width=8.3cm]{fig-predict-runtime-big.pdf}

\end{center}
\caption{{\label{fig:baseline-runtime}}  Prediction accuracy for PP and iMRU (top) and  execution time for TCE, PP, and iMRU (bottom).}
\end{figure}

\begin{figure}[!ht]
\begin{center}
\includegraphics[width=8.5 cm]{fig-core-dynamic-2.pdf}


\includegraphics[width=8.5cm]{fig-edp-2.pdf}

\end{center}
\caption{{\label{fig:baseline-ed}}  Whole core dynamic energy saving (top, higher is better) and Energy delay product (bottom, lower is better)  for TCE, PP, and iMRU.}

\end{figure}

The prediction accuracy and execution time under PP and iMRU schemes are shown in Figure \ref{fig:baseline-runtime}, respectively. The performance of TCE is also included, but it always the same as the baseline. As shown in the Figure \ref{fig:baseline-runtime} (top), the prediction accuracy of the PP is quite high for most of the benchmarks. %
The  prediction accuracy for the more realistic approach, iMRU, is lower than PP, with 12\% difference. Way prediction requires re-accessing the cache way on a wrong prediction, incurring performance degradation. The average performance   degradations (shown in Figure \ref{fig:baseline-runtime} (bottom) ) are about 1.18\% and 5.02\% more execution time for PP and iMRU, respectively. And the worst cases are 3.78\% and 14.57\% for PP and iMRU, respectively.  


\subsubsection{ Energy delay product (EDP).}

The whole core dynamic energy saving and corresponding EDP are illustrated in Figure \ref{fig:baseline-ed}. The variation of these results comes from two aspects: (a) different cache dynamic energy saving for TCE and way prediction on different benchmarks; (b) cache dynamic energy accounts for different fractions of the whole core dynamic energy for different benchmarks. 
%As PP is ideal case, the following discussion will focus on the comparison between TCE and iMRU.

TCE dynamic energy saving comes from cache dynamic energy saving and DTLB dynamic energy saving. On contrast, way prediction (PP and iMRU) only saves dynamic energy on cache. Generally, the average whole core energy saving for TCE is almost the same as PP, better than iMRU. 
However, PP is not realistic, and TCE significantly outperforms iMRU on 21 benchmarks out of 27. 

On average, TCE outperforms both PP and iMRU in terms of EDP. Because of the delay caused by misprediction, TCE outperforms iMRU on \textit{ALL} cases. TCE has significant advantages over iMRU on several benchmarks, for example, \textit{mcf},  \textit{hmmer}, \textit{libquantum},  \textit{leslie3d}, \textit{dealII}, \textit{GemsFDT}. These big differences come from the miss prediction penalty for longer execution time.

The most significant advantage of TCE is that it reduces the cache dynamic energy without hurting performance across all the benchmarks. On the contrary, EDP for way prediction can be higher than the baseline system, such as \textit{libquantum} and \textit{GemsFDT}.

 



\section{Related work}\label{related_work}


The work most similar to TCE to reduce the cache way access is way prediction. Predictive sequential associative cache \cite{psac} uses a number of prediction sources to pick the first block in a set to probe. On a miss to the predicted way, the other ways are checked. Similar way prediction techniques have been proposed in \cite{wpsac} and \cite{pc_predict}. Powell et al. \cite{powell2001reducing} combined way prediction and selective direct-mapping to reduce L1 dynamic cache energy. Besides performance penalty on misprediction, physical address must be obtained from TLB to check if the prediction is correct.

Instead of prediction, way caching \cite{nicolaescu2003reducing} \cite{wcul2} records way number of recently accessed cache lines to reduce the dynamic energy of highly associative caches. A problem for this technique is that the way cache is accessed before cache access, which will add delay to the data access in the critical path.  A similar technique, way memoization \cite{way_memoization, way_mem_date}, links the cache line according to the instruction sequence.

Tagless cache \cite{sembrant_tagless} restructures the first-level cache and TLB for more efficient storage of tags, achieving substantial energy gains, but, unlike TCE, does not elide TLB accesses, requires changes to the replacement policy, affects miss rates and performance in unpredictable ways, and complicates support for coherence and virtual address synonyms.

Before accessing the cache, techniques are proposed to filter unnecessary accesses. Sentry tag \cite{sentry_tag} determines the mismatched ways and halts them to save energy. The way halting technique \cite{zhang2005way} is an extension of the concept of sentry tags. Way decay \cite{way_decay} and way guard \cite{way_guard} adopt a bloom filter to reduce the ways that need to be checked. One problem for this type of techniques is that a new structure must be accessed serially, after address generation before accessing the cache, hence either increasing cycle time or adding a pipestage to the memory access latency.

\section{Conclusion} \label{conclusion}
 
To reduce set-associative cache access energy, we propose Tag Check Elision, which avoids tag checks and TLB access without causing performance degradation. TCE memoizes the cache line that has been accessed and the offset of the memory address for a base address. Access to the same line elide tag checks and TLB lookups by comparing the offset of the memory address that use the same base register. To improve the base design, three optimizations are proposed, namely double records, register value tracking and a backup buffer. 

 

We evaluated the effectiveness of the base TCE approach and further optimizations on X86  for reducing L1 data cache energy. The TCE approach avoids 35\% to 86\%  of tag checks, which results in data cache energy savings of 15\% to 56\%, and DTLB dynamic energy saving of 34\% to 85\%. Under the TCE approach, set-associative caches achieve better energy delay product than way prediction.


{\scriptsize
\bibliographystyle{abbrv}
\bibliography{references} 
}


\end{document}
